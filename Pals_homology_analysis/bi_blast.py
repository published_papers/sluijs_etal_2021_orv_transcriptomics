#!/usr/bin/env python3

import pandas as pd
import numpy
from subprocess import run

def Shell(cmd):
    return run(cmd, shell=True, check=True)

if __name__ == "__main__":
    # Strain name of reference
    refName = "N2"
    candidates = "Data/pals.fasta"

    # Load genome information
    genomes = pd.read_csv("Data/genomes.csv", index_col="Strain")

    # Build blast database
    Shell("mkdir -p Nosync/Blast")
    for current in genomes.index:
        print(f"Builidng db for {current}")

        fasta = genomes["Genome"].loc[current]
        db = f"Nosync/Blast/{current}"
        log = f"Nosync/Blast/{current}.log"

        Shell(f"zcat {fasta} | makeblastdb -dbtype nucl -parse_seqids \
                -out {db} -title '{current}' > {log} 2>&1")

    # Split
    reference = genomes.loc[refName]
    targets = genomes.drop(reference.name)

    # Bi-directional BLASTn
    tmpFile = "Nosync/my_query.fa"
    refDb = f"Nosync/Blast/{refName}"

    # Blast settings
    blastCols = ["qseqid", "sseqid", "length", "qstart",
                 "qend", "sstart", "send", "qcovs", "pident",
                 "evalue", "bitscore", "qseq", "sseq"]
    blastFmt = "7 " + " ".join(blastCols)

    Shell("mkdir -p Results")
    for current in targets.index:
        # Forward blast
        print(f"Blasting candidates to {current}")

        db = f"Nosync/Blast/{current}"
        fwdFile = f"Results/{current}_fwd.tsv"

        run(["blastn", "-db", db, "-query", candidates,
             "-outfmt", blastFmt, "-out", fwdFile], check=True)

        # Load blast results and take all hits within 10% bitscore of top hit
        results = pd.read_table(fwdFile, names=blastCols, comment="#")

        def FracSel(row, df, on = "bitscore", frac = 0.95, group = "qseqid"):
            current = row[on]
            scores = df[on].loc[df[group] == row[group]]
            return current > frac * max(scores)

        idx = [FracSel(row, results) for idx, row in results.iterrows()]

        # Write matching sequences to fasta
        with open(tmpFile, "w") as f:
            for idx, row in results[idx].iterrows():
                name = row["qseqid"]
                seq = row["sseq"].replace("-", "")
                f.write(f">{name}_Hit{idx}\n{seq}\n")

        # Reverse Blast
        print(f"Blasting candidate hits from {current} to {refName}")

        revFile = f"Results/{current}_rev.tsv"

        run(["blastn", "-db", refDb, "-query", tmpFile,
             "-outfmt", blastFmt, "-out", revFile], check=True)

    Shell("gzip -f Results/*.tsv")
