#!/usr/bin/env bash

# Align using clustal omega
cd Results/Alignment
parallel clustalo -i {} --full -o {.}.aln ::: *.fasta

# OPTIONAL: determine the best model to use with modeltest
# parallel ../../Bin/modeltest-ng_0.1.7 -i {} -t ml ::: *.aln

# Build tree using RAxML
cd ..
mkdir -p RAxML
cd RAxML
parallel raxmlHPC-SSE3 -s {} -c 1 -m GTRCATX -n {/.} -N 100 -x 123 -p 123 -k -f a ::: ../Alignment/*.aln
